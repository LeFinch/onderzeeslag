<?php


use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;
use Ratchet\Http\HttpServer;   


spl_autoload_register(function($class) {
    if(file_exists("./lib/" . $class . ".php"))
    include_once "./lib/" . $class . ".php";
});

require dirname(__FILE__) . '/vendor/autoload.php';

function num2alpha($n)
{
    for($r = ""; $n >= 0; $n = intval($n / 26) - 1)
        $r = chr($n%26 + 0x41) . $r;
    return $r;
}

$server = IoServer::factory(
    new HttpServer(new WsServer(new Game())),
    8089
);

$server->run();