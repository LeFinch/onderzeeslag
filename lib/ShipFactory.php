<?php

class ShipFactory {
    public static function create($type, Coord $coord, $orientation) {
        switch($type) {
            case Ship::TYPE_1:
                return new Ship($coord, $orientation, 5);
                break;
            case Ship::TYPE_2:
                return new Ship($coord, $orientation, 4);
                break;
            case Ship::TYPE_3:
                return new Ship($coord, $orientation, 3);
                break;
            case Ship::TYPE_4:
                return new Ship($coord, $orientation, 3);
                break;
            case Ship::TYPE_5:
                return new Ship($coord, $orientation, 2);
                break;
        }
        return false;
    }
}