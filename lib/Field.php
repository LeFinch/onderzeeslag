
<?php

class Field {
    protected $grid_size = 50;
    protected $ships = array();
    protected $coords = array();
    public function __construct() {
        $grid_size = $this->getGridSize();
        $k = 0;
        for($i = 1; $i <= $grid_size; $i++) {
            for($j = 1;$j <= $grid_size; $j++, $k++) {
                $coord = num2alpha(25 + $j);
                $coord_object = new Coord($this, $k, $coord . str_pad($i, 2, "0", STR_PAD_LEFT));
                $this->coords[] = $coord_object;
            }
        }
    }
    public function addShip(Ship $ship) {
        $this->ships[] = $ship;
    }
    public function getShips() {
        return $this->ships;
    }
    public function getGridSize() {
        return $this->grid_size;
    }

    public function getCoordByKey($key) {
        return $this->coords[$key];
    }
    public function getCoordByName($name) {
        foreach ($this->coords as $key => $coord) {
            if($coord->getName() == $name) {
                return $coord;
            }
        }
        return false;
    }
    public function getCoords() {
        return $this->coords;
    }
}

