<?php

class Orientation {
    const NORTH = 1;
    const EAST = 2;
    const SOUTH = 3;
    const WEST = 4;
    const NORTHEAST = 5;
    const SOUTHEAST = 6;
    const SOUTHWEST = 7;
    const NORTHWEST = 8;
}