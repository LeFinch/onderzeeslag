<?php


use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
class Game implements MessageComponentInterface {
    
    const STATE_WAITING_FOR_PLAYERS = 1;
    const STATE_PLACING_SHIPS = 2;
    const STATE_MAIN_GAME = 3;
    const STATE_FINISHED = 4;
    
    protected $players = array();
    protected $playing_players = array();
    protected $field;
    protected $state;
    protected $turn = 0;
   
    public function __construct() {
        $this->state = self::STATE_WAITING_FOR_PLAYERS;
        $this->field = new Field;
    }
    public function getState() {
        return $this->state;
    }
    public function setState($state) {
        $this->state = $state;
    }
    public function getPlayer(ConnectionInterface $conn) {
        foreach ($this->players as $player) {
            if($player->getConnection() === $conn) {
                return $player;
            }
        }
        return false;
    }
    public function getPlayers() {
        return $this->players;
    }
    public function getPlayingPlayers() {
        return $this->playing_players;
    }
    public function getField() {
        return $this->field;
    }
    public function onOpen(ConnectionInterface $conn) {
        $player = new Player($conn, $this);
        $this->players[] = $player;
        $player->sendWelcome();
        $this->sendGameState();
    }
    public function onMessage(ConnectionInterface $conn, $msg) {
        $player = $this->getPlayer($conn);
        $command = json_decode($msg);
        switch($command->action) {
            case 'shoot':
                if(!$this->isPlayersTurn($player)) {
                    return;
                }
                if($this->state != self::STATE_MAIN_GAME) {
                    return;
                }
                $coord_object = $this->field->getCoordByName($command->coords);
                $coord_object->setIsShot(true);
                $hit = $coord_object->isHit();
                $this->sendShot($player, $command->coords, $hit);
                echo "Player " . $player->getID() . " shoots at " . $command->coords . ": " . ($hit ? 'hit':'miss') . PHP_EOL;
                $this->turn++;
                $this->sendTurnInfo();
                break;
            case 'name':
                if($this->state != self::STATE_WAITING_FOR_PLAYERS) {
                    return;
                }
                $player->setName($command->name);
                echo "Player " . $player->getID() . " set name to ". $player->getName() . PHP_EOL;
                break;
            case 'ready':
                $player->setIsReady($command->is_ready);
                echo "Player " . $player->getID() . " / ". $player->getName() . " is " . ($player->isReady()?'ready' : 'not ready') . PHP_EOL;
                break;
            case 'message':
                if($command->to) {
                    $receiver = $this->getPlayerById($command->to);
                    $receiver->sendChatMessage($player, $command->message, true);
                } else {
                    $this->sendChatMessage($player, $command->message);
                }
                echo "Player " . $player->getID() . " / ". $player->getName() . " sent message: " . $command->message . PHP_EOL;
                return;
                break;
            case 'place_ship':
                if($this->state != self::STATE_PLACING_SHIPS) {
                    return;
                }
                if($player->hasPlacedShips()) {
                    return;
                }
                $ship = ShipFactory::create($command->type, $this->field->getCoordByName($command->coords), $command->orientation);
                $player->addShip($ship);
                $this->field->addShip($ship);
                echo "Player " . $player->getID() . " placed ship ". PHP_EOL;
                break;
            case 'transition':
                switch($this->getState()) {
                    case Game::STATE_WAITING_FOR_PLAYERS:
                        if(count($this->players) > 1) {
                            $ready = true;
                            foreach($this->getPlayers() as $player) {
                                if(!$player->isReady()) {
                                    $ready = false;
                                }
                            }
                            if($ready === true) {
                                $this->setState(Game::STATE_PLACING_SHIPS);
                                $this->playing_players = $this->players;
                            } 
                        }
                        break;
                    case Game::STATE_PLACING_SHIPS:
                        $placed_ships = true;
                        foreach($this->getPlayingPlayers() as $player) {
                            if(!$player->hasPlacedShips()) {
                                $placed_ships = false;
                            }
                        }
                        if($placed_ships === true) {
                            $this->setState(Game::STATE_MAIN_GAME);
                            $this->sendTurnInfo();
                        }
                        break;
                }
                break;
        }
        if($this->state == Game::STATE_MAIN_GAME) {
            $player_alive = 0;
            foreach ($this->playing_players as $key => $player) {
                if(!$player->isDead()) {
                    $player_alive++;
                }
            }
            if($player_alive < 2) {
                $this->setState(Game::STATE_FINISHED);
            }
        }
        $this->sendGameState();
    }
    public function onClose(ConnectionInterface $conn) {
        $disconnected_player = $this->getPlayer($conn);
        if(!$disconnected_player) {
            return;
        }
        foreach ($this->players as $key => $player) {
            $player->sendDisconnect($disconnected_player);
            if($player === $this->getPlayer($conn)) {
                $this->players[$key] = null;
                $this->players = array_filter($this->players);
            }
        }
    }
    public function onError(ConnectionInterface $conn, \Exception $e) {
        
    }
    public function getCurrentTurnPlayer() {
        return $this->playing_players[($this->turn % count($this->playing_players))];
    }
    public function isPlayersTurn(Player $player) {
        return ($this->getCurrentTurnPlayer() === $player);
    }
    public function sendNames() {
        
    }
    public function sendChatMessage(Player $by, $message) {
        foreach ($this->players as $player_object) {
            $player_object->sendChatMessage($by, $message, false);
        }
    }
    public function sendTurnInfo() {
        foreach ($this->players as $player_object) {
            $player_object->sendTurnInfo($this->getCurrentTurnPlayer());
        }
    }
    public function sendGameState() {
        foreach ($this->players as $player_object) {
            $player_object->sendGameState();
        }
    }
    public function sendShot(Player $player, $coords, $hit) {
        foreach ($this->players as $player_object) {
            $player_object->sendShot($player, $coords, $hit);
        }
    }
}