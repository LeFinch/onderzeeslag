<?php 


class Coord {
    
    protected $field;
    protected $key;
    protected $name;
    protected $shot = false;
    public function __construct(Field $field, $key, $name) {
        $this->field = $field;
        $this->key = $key;
        $this->name = $name;
    }
    
    public function setIsShot($shot) {
        $this->shot = $shot;
    }
    
    public function getName() {
        return $this->name;
    }
    
    public function getNextCoord($orientation) {
        switch($orientation) {
            case Orientation::NORTH:
                return $this->field->getCoordByKey( $this->key - $this->field->getGridSize() );
                break;
            case Orientation::EAST:
                return $this->field->getCoordByKey( $this->key + 1 );
                break;
            case Orientation::SOUTH:
                return $this->field->getCoordByKey( $this->key + $this->field->getGridSize() );
                break;
            case Orientation::WEST:
                return $this->field->getCoordByKey( $this->key - 1 );
                break;
            case Orientation::NORTHEAST:
                return $this->field->getCoordByKey( $this->key + 1 - $this->field->getGridSize() );
                break;
            case Orientation::SOUTHEAST:
                return $this->field->getCoordByKey( $this->key + 1 + $this->field->getGridSize() );
                break;
            case Orientation::SOUTHWEST:
                return $this->field->getCoordByKey( $this->key - 1 + $this->field->getGridSize() );
                break;
            case Orientation::NORTHWEST:
                return $this->field->getCoordByKey( $this->key - 1 - $this->field->getGridSize() );
                break;
        }
        return false;
    }
    
    public function isHit() {
        if(!$this->isShot()) {
            return false;
        }
        foreach ($this->field->getShips() as $ship) {
            if($ship->occupies($this)) {
                return true;
            }
        }
        return false;
    }
    
    public function isShot() {
        return $this->shot;
    }

    public function getCoordsInRadius($radius) {
        $coords = array();
        $coords[] = $this;
        $pick_last_box = (($radius % 2) == 0);
        for($d = 1; $d < 5; $d++) {
            $direction_normal = $d;
            $direction_bend = ($d % 4) + 1 ;
            $depth = $radius;
            $direction = $direction_normal;
            $base_pointer = $this;
            $last_coord = null;
            for ($i = 0; $depth > 0; $i++) {
                $last_coord = $base_pointer;
                for ($j = 0; $j < $depth; $j++) {
                    $last_coord = $coords[] = $last_coord->getNextCoord($direction);
                    if ($j == 0) {
                        $base_pointer = $last_coord;
                    }
                }
                if ($direction == $direction_normal) {
                    $direction = $direction_bend;
                } else {
                    $direction = $direction_normal;
                }
                if($i != 0 AND $i != 4) {
                    $depth--;
                }
            }

        }

        return $coords;
    }

    public function getRandomCoordsInDirection($direction, $amount = 10) {
        $coords = array();
        $coords[] = $this;
        $last_coord = $this;
        for($i = 0;$i < $amount;$i++) {
            $last_coord = $last_coord->getNextCoord($direction);
            $rand = mt_rand(0, 4);
            if($rand == 0) {
                $coords[] = $last_coord;
            } else {
                $coords[] = $last_coord->getNextCoord($rand);
            }
        }
        return $coords;
    } 
    
}
