<?php


use Ratchet\ConnectionInterface;

class Player {

    const SHIP_AMOUNT = 5;

    protected $conn;
    /**
     * @var Game
     */
    protected $game;
    protected $id;
    protected $name;
    protected $is_ready = false;
    protected $ships = array();
    public function __construct(ConnectionInterface $conn, Game $game) {
        $this->conn = $conn;
        $this->game = $game;
        $this->id = uniqid();
        $this->setName('Player ' . $this->id);
    }
    public function getID() {
        return $this->id;
    }
    public function getConnection() {
        return $this->conn;
    }
    public function sendWelcome() {
        $this->conn->send(json_encode(array(
            'action' => 'welcome',
            'you' => $this->getID(),
            'message' => 'Onderzeeslag! :)'
        )));
    }
    public function canMove() {
        return true;
    }
    public function setName($name) {
        $this->name = $name;
        $this->game->sendNames();
    }
    public function setIsReady($ready) {
        $this->is_ready = $ready;
    }
    public function isReady() {
        return $this->is_ready;
    }
    public function getName() {
        return $this->name;
    }
    public function hasPlacedShips() {
        return (count($this->ships) == self::SHIP_AMOUNT);
    }
    public function isDead() {
        foreach ($this->ships as $key => $ship) {
            if(!$ship->isWrecked()) {
                return false;
            }
        }
        return true;
    }
    public function sendShot(Player $player, $coords, $hit) {
        $json = array(
            'action' => 'shoot',
            'player' => $player->getID(),
            'coords' => $coords,
            'hit' => $hit,
            'tiles' => array(
                array('coords' => $coords, 'shot' => 1, 'hit' => $hit)
            )
        );
        
        $this->conn->send(json_encode($json));
    }
    public function addShip(Ship $ship) {
        $this->ships[] = $ship;
    }
    public function getShips() {
        return $this->ships;
    }
    public function sendName(Player $player) {
        $json = array(
            'action' => 'name_update',
            'player' => $player->getID(),
            'name' => $player->getName(),
        );
        $this->conn->send(json_encode($json));
    }
    public function sendChatMessage(Player $player, $message, $private = false) {
        $json = array(
            'action' => 'message',
            'player' => $player->getID(),
            'private' => $private,
            'message' => $message,
        );
        $this->conn->send(json_encode($json));
    }
    public function sendDisconnect(Player $player) {
        $json = array(
            'action' => 'disconnect',
            'player' => $player->getID(),
        );
        $this->conn->send(json_encode($json));
    }
    public function sendTurnInfo(Player $player) {
        $json = array(
            'action' => 'turn',
            'player' => $player->getID(),
        );
        $this->conn->send(json_encode($json));
    }
    public function sendGameState() {
        $json = array(
            'action' => 'game',
            'state' => $this->game->getState(),
        );
        $players = array();
        foreach($this->game->getPlayers() as $player) {
            $players[] = array(
                'id' => $player->getID(),
                'name' => $player->getName(),
                'ready' => $player->isReady(),
                'dead' => $player->isDead(),
                'ships_placed' => count($player->getShips()),
            );
        }
        $json['players'] = $players;
        $json['grid_size'] = $this->game->getField()->getGridSize();
        
        $tiles = array();
        foreach($this->game->getField()->getCoords() as $coord) {
            $tile = array('coords' => $coord->getName());
            if($coord->isShot()) {
                $tile['shot'] = true;
            }
            if($coord->isHit()) {
                $tile['hit'] = true;
            }
            foreach ($this->ships as $ship) {
                if($ship->occupies($coord)) {
                    $tile['ship'] = true;
                }   
            }
            if(count($tile) > 1) {
                $tiles[] = $tile;
            }
        }
            
        $json['tiles'] = $tiles;

        $this->conn->send(json_encode($json));
    }
}
