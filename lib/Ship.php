<?php


class Ship {

    const TYPE_1 = 1;
    const TYPE_2 = 2;
    const TYPE_3 = 3;
    const TYPE_4 = 4;
    const TYPE_5 = 5;

    /**
     * @var Coord
     */
    protected $coord;
    protected $orientation;
    protected $length;

    public function __construct(Coord $coord, $orientation, $length)
    {
        $this->coord = $coord;
        $this->orientation = $orientation;
        $this->length = $length;
    }

    public function occupies(Coord $coord) {
        foreach($this->getCoords() as $stored_coord) {
            if($stored_coord === $coord) {
                return true;
            }
        }
        return false;
    }
    
    public function isWrecked() {
        $parts_alive = 0;
        foreach ($this->getCoords() as $key => $coord) {
            if(!$coord->isHit()) {
                $parts_alive++;
            }
        }
        return ($parts_alive == 0);
    }
    
    public function getCoords() {
        $coords = array();
        $coords[] = $pointer = $this->coord;
        for($i = 0;$i < $this->length - 1;$i++) {
            $coords[] = $pointer = $pointer->getNextCoord($this->orientation);
        }
        return $coords;
    }
    
    public function explode() {
        foreach ($this->getCoords() as $key => $coord) {
            foreach($coord->getCoordsInRadius(rand(2,4)) as $hit_coord) {
                $hit_coord->setIsShot(true);
            }
        }
    }

}
